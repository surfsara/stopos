
/*
   Copyright 2013 Willem Vermin, SURFsara

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
#include <string>
#include <vector>
#include <string.h>
#include <iostream>
#include <algorithm>
#include <sys/time.h>
#include "wutils.h"
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include <stdlib.h>
#include "wtd.h"

// 
// splits string s to vector v, using separator character sep
// return value: v
// It seems that it follows the python implementation of split()
//
std::vector <std::string> split(std::vector <std::string> &v, const std::string s, const char sep)
{
  v.clear();
  for (size_t p=0, q=0; q != s.npos; p = q + 1)
    v.push_back(s.substr(p,(q=s.find(sep,p))-p));
  return v;
}

// converts string to char *
// resulting string is created using malloc. Use free to free.
char* str_to_charp(const std::string s)
{
  return strdup(s.c_str());
}

/*
 * converts string to hex representation
 */
std::string strtohex(const std::string s)
{
  std::string a;
  const std::string c=
  "000102030405060708090a0b0c0d0e0f"
  "101112131415161718191a1b1c1d1e1f"
  "202122232425262728292a2b2c2d2e2f"
  "303132333435363738393a3b3c3d3e3f"
  "404142434445464748494a4b4c4d4e4f"
  "505152535455565758595a5b5c5d5e5f"
  "606162636465666768696a6b6c6d6e6f"
  "707172737475767778797a7b7c7d7e7f"
  "808182838485868788898a8b8c8d8e8f"
  "909192939495969798999a9b9c9d9e9f"
  "a0a1a2a3a4a5a6a7a8a9aaabacadaeaf"
  "b0b1b2b3b4b5b6b7b8b9babbbcbdbebf"
  "c0c1c2c3c4c5c6c7c8c9cacbcccdcecf"
  "d0d1d2d3d4d5d6d7d8d9dadbdcdddedf"
  "e0e1e2e3e4e5e6e7e8e9eaebecedeeef"
  "f0f1f2f3f4f5f6f7f8f9fafbfcfdfeff";

  a.resize(2*s.size());
  int k = 0;
  for (unsigned int i=0; i<s.size(); i++)
  {
    int p  = 2*(unsigned char)s[i];
    a[k++] = c[p++];
    a[k++] = c[p];
  }

  return a;
}

/*
 * convert hex represented string to string
 * unknown hex numbers result in '?'
 */
std::string hextostr(const std:: string s)
{
const char c[] = {
  -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
  -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
  -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
   0, 1, 2, 3, 4, 5, 6, 7, 8, 9,-1,-1,-1,-1,-1,-1,
  -1,10,11,12,13,14,15,-1,-1,-1,-1,-1,-1,-1,-1,-1,
  -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
  -1,10,11,12,13,14,15,-1,-1,-1,-1,-1,-1,-1,-1,-1,
  -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
  -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
  -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
  -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
  -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
  -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
  -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
  -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
  -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
               };

  std::string a;
  unsigned int l = s.size();
  a.resize((l+1)/2);

  unsigned int k=0;
  unsigned int l2 = (l/2)*2;
  for (unsigned int i=0; i<l2; i+=2)
  {
    int p = c[(unsigned int)s[i]];
    int q = c[(unsigned int)s[i+1]];
    if (p >= 0 && q >= 0)
      a[k++] = 16*p + q;
    else
      a[k++] = '?';
  }

  if (l2 < l)
    a[k] = '?';

  return a;

}

std::string zstrtohex(const std::string s)
{
  return ZBEGIN_+strtohex(s)+ZEND_;
}

std::string zhextostr(const std::string s)
{
  size_t lb = ZBEGIN_.size();
  size_t le = ZEND_.size();
  size_t ls = s.size();

  if (ls < lb + le)
    return s;

  if (s.substr(0,lb) != ZBEGIN_)
    return s;

  if (s.substr(ls-le) != ZEND_)
    return s;

  return hextostr(s.substr(lb,ls-lb-le));
}

//generates random passwoord of length len
std::string gen_random(const int len) {
 timeval t1;
 gettimeofday(&t1, NULL);
 srand(t1.tv_usec * t1.tv_sec);
 static const char alphanum[] =
   "0123456789"
   "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
   "abcdefghijklmnopqrstuvwxyz";

 std::string s;
 s.resize(len);
 for (int i = 0; i < len; ++i) 
 {
   s[i] = alphanum[std::rand() % (sizeof(alphanum) - 1)];
 }
 return s;
}

#include <fstream>
#include <string>
#include <cerrno>

int get_file_contents(std::string &contents,const std::string &filename)
{
  std::ifstream in(filename.c_str(), std::ios::in | std::ios::binary);
  if (in)
  {
    in.seekg(0, std::ios::end);
    contents.resize(in.tellg());
    in.seekg(0, std::ios::beg);
    in.read(&contents[0], contents.size());
    in.close();
    return(0);
  }
  return -1;
}

// tries to mimic mkdir -p
int mkdir_p(const std::string &d,mode_t mode)
{
  size_t p=0;

  std::string s = d;
  if (s[s.size()-1] != '/')
    s += '/';

  int rc=0;
  std::string t;
  while (( p = s.find_first_of('/',p)) != s.npos)
  {
    p++;
    t = s.substr(0,p);
    rc = mkdir (t.c_str(),mode);
    if (rc < 0)
    {
      if (errno == EEXIST)
      {
	struct stat buf;
	rc = lstat(t.c_str(),&buf);
	if (rc < 0)
	  break;
	if (!S_ISDIR(buf.st_mode))
	{
	  rc = -1;
	  break;
	}
      }
      else
	break;
    }
    if ( p == s.size())
      break;
  }
  return rc;
}

std::string envtostr(const std::string &s)
{
  char *a = getenv(s.c_str());
  if (a == 0)
    return "";

  return a;
}

int get_dir_list(std::list <std::string> &v,
                 const std::string       &d)
{
  DIR *dir;
  struct dirent *ent;
  dir = opendir(d.c_str());
  if (dir == 0)
    return 1;

  while ((ent = readdir (dir)) != 0) 
    v.push_back(ent->d_name);
  closedir (dir);
  return 0;
}

size_t count_lines(std::ifstream &f) { 
    int number_of_lines = 0;
    std::string line;

    std::streampos p = f.tellg();

    while (std::getline(f, line))
        ++number_of_lines;
    f.clear();
    f.seekg(p);
    return number_of_lines;
}

#include <sys/times.h>
#include <sys/time.h>
#include <time.h>
#include <stdlib.h>
#include "clocks.h"
#include <unistd.h>

double progress_bar::wallc()
{
  struct timeval t;

  gettimeofday(&t,0);
  return t.tv_sec+0.000001*(double)t.tv_usec;
}

progress_bar::progress_bar()
{
  this->clear();
  this->timer        = 0.3;
  this->bar_length   = 60;
}
 
void progress_bar::clear()
{
  this->firsttime  = 1;
  this->walltime   = wallc();
}

void progress_bar::set_timer(float t)
{
  this->timer = t;
}

std::string progress_bar::show(size_t num, double frac)
{
  bool c;
  return this->show(num,frac,c);
}

std::string progress_bar::show(size_t num, double frac, bool &changed)
{
  changed = 0;
  if (!this->firsttime)
  {
    if (this->wallc() - this->walltime < this->timer)
      return this->pshow;
  }

  changed = this -> firsttime || this->pfrac != frac || this->pnum != num;

  if (! changed)
    return this->pshow;

  this->firsttime = 0;

  int f = 100*frac+0.5;

  std::string b((size_t)(frac*this->bar_length),'x');
  b.resize(this->bar_length,'-');

  this->pshow = NumberToString(num) + ' ' + NumberToString(f) + "% ["
                + b + ']';

  this->pnum     = num;
  this->pfrac    = frac;
  this->walltime = this->wallc();

  return this->pshow;
  
}

std::string progress_bar::show(size_t num)
{
  bool c;
  return this->show(num,c);
}

std::string progress_bar::show(size_t num, bool &changed)
{
  changed = 0;
  if (!this->firsttime)
  {
    if (this->wallc() - this->walltime < this->timer)
      return this->pshow;
  }

  changed = this -> firsttime || this->pnum != num;

  if (! changed)
    return this->pshow;

  this->firsttime = 0;

  this->pshow     = NumberToString(num);
  this->pnum      = num;
  this->walltime  = this->wallc();

  return this->pshow;
  
}
