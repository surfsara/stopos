# Welcome to stopos

## What is stopos

Stopos is a tool to manage large number of tasks, mostly started in jobs. 

First, you define a 'pool' and fill it with lines (command lines, parameters, ...).
Then, tasks (usually in jobs) ask stopos for a line. Stopos takes care that lines that are 
committed, will not be committed again, unless the user specifically requires to re-commit lines.

### Typical usage

* The user fills a pool with lines, consisting of parameters that are to be used by a program.
* In a job: as many tasks as there are cores are started in parallel. 
* Each task asks stopos a line: stopos will give each task another line.
* The task executes the required command using the line as parameter.
* The task tells stopos that this line is now invalidated, it must not be committed ever again.

### Stopos is server-based

Stopos is based on a client-server model.
The clients runs on the compute cluster, the server runs on a server node.

### Licence

Apache 2.0 2004

## Download and install

### Requirements

Debian:
 * zlib1g-dev
 * libcurl4-openssl-dev
 * g++
 * make
 * git
 * libgdbm-dev

SLES/RHEL:
 * zlib-devel
 * libcurl-devel
 * git-core
 * gcc-c++
 * make
 * gdbm-devel


### Download
```
git clone https://gitlab.com/surfsara/stopos.git
cd stopos
```

### Compile
```
# edit the make-script Make.inc to your taste
./makeit
```

After succesfull compilation, there are:

* ```stoposclient```, the client program
* ```stoposserver```, the server program
* ```stoposzztostr```, a program that takes on stdin a hex-encoded string (with pre-and postfix) and outputs the decoded string. 
  This program can be useful when looking at the files on the server side.
* ```stoposdump```, a script that demonstrates the use of 'stopos dump'.
* a script ```makefunction``` that creates a shell function called ```stopos```. This shell function is normally used when invoking the program. It takes care of setting environment variables.
* a man page: ```stopos.1``` which describes the usage, also containing an example.
* ```modulefile```: an example of a modulefile to activate the stopos command. (Only useful if you use modules, but otherwise, it illustrates how to create a suitable alias (called ```stopos```)) for the C-shell
* a comprehensive description of the program in ```doc/description.pdf```

### More documentation

We have more [user documentation](https://servicedesk.surf.nl/wiki/display/WIKI/Using+STOPOS) on SURF's website.

