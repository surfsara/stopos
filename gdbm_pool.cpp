
/*
   Copyright 2013 Willem Vermin, SURFsara

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
#include <iostream>
#include <string>
#include <sstream>
#include <iomanip>
#include "stopos_pool.h"
#include "gdbm_pool.h"
#include "gdbm.h"
#include <string.h>
#include <stdlib.h>
#include "wutils.h"
#include <algorithm>
#include <vector>
#include "stopos.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

gdbm_pool::gdbm_pool()
{
  this->whoami = "gdbm";
}

gdbm_pool::~gdbm_pool()
{
}

int gdbm_pool::create_db(const std::string &dbname)
{
  int rc;

  rc = this->purge_db();
  if (rc != 0)
    return rc;

  // The db_name can be composed of parts separated with '/'
  // we try to create this path until the last '/'
  
  size_t p = dbname.find_last_of('/');
  rc = mkdir_p(dbname.substr(0,p+1),0755);
  if (rc != 0)
    return CREATEERROR;

  this->dbf = gdbm_open(this->db_name,0,GDBM_NEWDB|this->sync|GDBM_NOLOCK,0644,0);

  if (this->dbf == 0)
    return this->OPENERROR;
  this->close_db();

  return 0;
}

int gdbm_pool::purge_db(void)
{
  if (this->db_name == 0)
    return NOFILENAME;

  this->close_db();

  unlink(db_name);

  return 0;
}


int gdbm_pool::open_db(void)
{

  if (this->db_open)
    return 0;

  if (this->db_name == 0)
    return this->NOFILENAME;

  // not really clear how gdbm signals that a file is not present
  // so we open de file using open, and close it in close_db
  //
  this->ff = open(db_name,O_RDWR);
  if (this->ff < 0)
    return OPENERROR;

  // only 'w' is used, left the other part in for future
  // reference

  // gdbm does not proper locking, somehow.
  // so we use fcntl to get a lock on the file

  struct flock lock;
  lock.l_type   = F_RDLCK|F_WRLCK;
  lock.l_whence = SEEK_SET;
  lock.l_start  = 0;
  lock.l_len    = 0;

  int rc;

  rc = fcntl(this->ff,F_SETLKW,&lock);
  if (rc < 0)
    return LOCKERROR;

  char rw='w';
  switch (rw)
  {
    case 'r':
      do
      {
        dbf = gdbm_open(this->db_name,0,GDBM_READER,0644,0);
        if (dbf == 0)
          usleep(1000);
      }
      while (dbf == 0);
      break;
    case 'w':
      do
      {
        dbf = gdbm_open(this->db_name,0,GDBM_WRCREAT|this->sync,0644,0);
        if (dbf == 0)
          usleep(1000);
      }
      while (dbf == 0);
    break;
    default:
      break;
  }
  this->db_open = 1;
  return 0;
}

int gdbm_pool::close_db(void)
{
  if (!db_open) 
    return 0;

  gdbm_close(this->dbf);

  int rc;

  struct flock lock;
  lock.l_type   = F_UNLCK;
  lock.l_whence = SEEK_SET;
  lock.l_start  = 0;
  lock.l_len    = 0;

  rc = fcntl(this->ff,F_SETLKW,&lock);
  if (rc < 0)
    return UNLOCKERROR;

  rc = close(this->ff);
  if (rc == -1)
    return CLOSEERROR;

  this->db_open = 0;

  return 0;
}

int gdbm_pool::get_record(std::string &r, const std::string &k, const std::string &l)
{
  datum key,content;
  key.dptr = str_to_charp(k);
  key.dsize = strlen(key.dptr) + 1;
  content=gdbm_fetch(this->dbf,key);
  free(key.dptr);
  if (content.dptr == 0)
    return this->FETCHERROR;

  r = content.dptr;

  free(content.dptr);

  return 0;
}

int gdbm_pool::put_record(const std::string &r, const std::string &k, const std::string &l)
{

  datum key, content;
  key.dptr = str_to_charp(k);
  key.dsize = strlen(key.dptr) + 1;

  content.dptr  = str_to_charp(r); 
  content.dsize = strlen(content.dptr)+ 1;
  bool rectoolong = 0;
  /*
  if (content.dsize > this->reclen)
  {
    rectoolong = 1;
    WTD(this->reclen<<" "<<content.dsize<<" "<<strlen(content.dptr));
    content.dsize = this->reclen;
    content.dptr[this->reclen-1] = '\0';
    WTD(this->reclen<<" "<<content.dsize<<" "<<strlen(content.dptr));
  }
  */
  int rc = gdbm_store(this->dbf, key, content, GDBM_REPLACE);
  free(content.dptr);
  free(key.dptr);
  if (!rectoolong)
    return rc;
  return this->RECORDTOOLONG;
}

int gdbm_pool::remove_record(const std::string &key, const std::string &l)
{
  datum k;
  int rc;
  k.dptr = str_to_charp(key);
  k.dsize = strlen(k.dptr) + 1;

  rc = gdbm_delete(this->dbf,k);
  if (rc !=0)
    return NOTFOUND;
  return 0;
}

