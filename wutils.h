
/*
   Copyright 2013 Willem Vermin, SURFsara

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
#ifndef WUTILS_H
#define WUTILS_H
#include <vector>
#include <string>
#include <sstream>
#include <iomanip>
#include <sys/types.h>
#include <list>
#include <dirent.h>
std::vector <std::string> split(std::vector <std::string> &v, const std::string s, const char sep);
char* str_to_charp(const std::string s);

template <typename T>
  std::string NumberToString ( T Number )
  {
    std::ostringstream ss;
    ss << Number;
    return ss.str();
  }
template <typename T>
  std::string NumberToString ( T Number,int w)
  {
    std::ostringstream ss;
    ss <<std::setfill('0') << std::setw(w) << Number;
    return ss.str();
  }
template <typename T>
  T StringToNumber ( const std::string &Text )
  {
    std::istringstream ss(Text);
    T result;
    return ss >> result ? result : 0;
  }

std::string strtohex(const std::string s);
std::string hextostr(const std::string s);
const static std::string ZBEGIN_ = "ZZZ";
const static std::string ZEND_   = "YY";

std::string zstrtohex(const std::string s);
std::string zhextostr(const std::string s);

std::string gen_random(const int len);

int get_file_contents(std::string &contents,const std::string &filename);

inline std::string trim_right(
  const std::string& s,
  const std::string& delimiters = " \f\n\r\t\v" )
{
  size_t p = s.find_last_not_of(delimiters);
  if (p == s.npos)
    return "";
  return s.substr( 0, p + 1 );
}

inline std::string trim_left(
  const std::string& s,
  const std::string& delimiters = " \f\n\r\t\v" )
{
  size_t p = s.find_first_not_of(delimiters);
  if (p == s.npos)
    return "";
  return s.substr(p);
}

inline std::string trim(
  const std::string& s,
  const std::string& delimiters = " \f\n\r\t\v" )
{
  return trim_left( trim_right( s, delimiters ), delimiters );
}

int mkdir_p(const std::string &s,mode_t m);

std::string envtostr(const std::string &s);

int get_dir_list(std::list <std::string> &v,
                 const std::string       &d);

size_t count_lines(std::ifstream &f);

class progress_bar
{
  private:
    double timer;
    double pfrac;
    size_t pnum;
    bool firsttime;
    double walltime;
    double wallc();
    std::string pshow;
    size_t bar_length;

  public:
    progress_bar();
    void clear();
    void set_timer(float t);
    std::string show(size_t num, double frac,bool &changed);
    std::string show(size_t num, double frac);
    std::string show(size_t num, bool &changed);
    std::string show(size_t num);

};
#endif
