
/*
   Copyright 2013 Willem Vermin, SURFsara

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
#ifndef STOPOS_H
#define STOPOS_H
#include "wtd.h"
static const char dbsep           = '/';  // separator used in database
static const char httpsep         = '-';  // separator used in http string from client to server
static const char outsep          = '+';  // separator used in output from server to client
static const char keysep          = '.';  // separator to separate stopos
                                          // key from extra key
const std::string program_name    = "stopos";
const std::string program_version = "0.93";
const std::string default_pool    = "pool";

typedef unsigned long long longuint;

const static size_t maxlinelength = 1024;

const std::string  connect_error_msg      = "ERROR: Cannot connect to server";
const unsigned int connect_error_retries  = 60;

#endif
