
/*
   Copyright 2013 Willem Vermin, SURFsara

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
#include <iostream>
#include <string>
#include "wutils.h"
int main()
{
  size_t p,q;
  size_t size_begin = ZBEGIN_.size();
  size_t size_end   = ZEND_.size();
  std::string line;

  while(std::getline(std::cin,line))
  {
    p = 0;
    q = 0;
    while(1)
    {
      q = line.find(ZBEGIN_,p);
      std::cout <<line.substr(p,q-p);
      //std::cout <<__LINE__<<":"<<line.substr(p,q-p)<<std::endl;
      if ( q == line.npos)
	break;
      else
      {
	p = q;
	q = line.find(ZEND_,p+size_begin);
	if (q == line.npos)
	{
	  std::cout <<line.substr(p);
	  break;
	}
	else
	{
	  std::cout <<zhextostr(line.substr(p,q-p+size_end));
	  p = q+size_end;
	}
      }
    }
    std::cout << std::endl;
  }
}
