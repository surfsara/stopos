# to be included in the Makefile
# Version of the software
STOPOS_VERSION=0.93

# installation directory of the server on local system:
CGI_INSTALL_DIR=$(HOME)/myroot/usr/local/www/cgi-bin

# installation directory of the server on the remote system
#REMOTE_CGI_INSTALL_DIR=stopos.osd.surfsara.nl:/usr/local/stopos/cgi-bin
REMOTE_CGI_INSTALL_DIR=localhost:/home/willem/www/cgi-bin

# location of the client programs:
CLIENT_INSTALL_DIR=$(HOME)/myroot/sara/sw/stopos-$(STOPOS_VERSION)

# remote installation directory of the client programs:
#REMOTE_CLIENT_INSTALL_DIR=stopos.osd.surfsara.nl:/usr/local/stopos/bin
REMOTE_CLIENT_INSTALL_DIR=localhost:/usr/local/stopos/bin

# location of the modulefile
MODULE_INSTALL_DIR=$(HOME)/mymodulefiles/64

# name of the c++ compiler, and it's flags
CXX=g++
CXXFLAGS=-Wall -O2

# Choose to generate one ore more of the following:
#MAKE_FLAT=yes
MAKE_GDBM=yes
#MAKE_MYSQL=yes
#MAKE_FILES=yes

# Choose here the default the client will choose:
#  this can be overwritten in run time by the environment
#   variable STOPOS_PROT
DB_DEFAULT=DEFAULT_GDBM
#DB_DEFAULT=DEFAULT_FILES
#DB_DEFAULT=DEFAULT_MYSQL
#DB_DEFAULT=DEFAULT_FLAT

# Define the default url of the server,
# can be overwritten in runtime by environment variable
# STOPOS_SERVER_URL
#SERVER_URL=http://localhost:5050/cgi-bin/stoposserver/
SERVER_URL=http://stopos.osd.surfsara.nl/cgi-bin/stoposserver

# Define directory to store the non-mysql databases 
# default is /tmp
# DB_DIR=/data/stopos

